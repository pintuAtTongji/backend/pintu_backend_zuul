package com.tongji.pintu.zuul.config;

import org.springframework.cloud.netflix.zuul.filters.Route;
import org.springframework.cloud.netflix.zuul.filters.RouteLocator;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * swagger文档资源配置类 （重点swagger整合zuul配置）
 */
@Component
@Primary
public class SwaggerDocsResourceConfig implements SwaggerResourcesProvider {
	private final RouteLocator routeLocator;

	public SwaggerDocsResourceConfig(RouteLocator routeLocator){
		this.routeLocator=routeLocator;
	}

	// 自动获取系统配置的路由资源集合
	@Override
	public List<SwaggerResource> get() {
		List<SwaggerResource> resources = new ArrayList<>();
		List<Route> routes = routeLocator.getRoutes();
		routes.forEach(route -> {
			resources.add(swaggerResource(route.getId(),
					route.getFullPath().replace("**", "v2/api-docs")));
		});
		return resources;
	}

	// 获取对应的路由资源
	private SwaggerResource swaggerResource(String name, String location) {
		SwaggerResource swaggerResource = new SwaggerResource();
		swaggerResource.setName(name);
		swaggerResource.setLocation(location);
		return swaggerResource;
	}
}