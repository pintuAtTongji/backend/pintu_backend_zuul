package com.tongji.pintu.zuul.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger配置类
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	//是否开启swagger
	@Value("${swagger.enable}")
	private boolean enableSwagger;

	@Bean
	public Docket createRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo())
				.enable(enableSwagger);
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("Pintu API文档")
				.description("Pintu接口文档大全")
				//.termsOfServiceUrl("http://localhost:8092")
				//.contact(new Contact("xx", "", "xxx@qq.com"))
				.version("1.0")
				.build();
	}

	@Bean
	UiConfiguration uiConfig() {
		return new UiConfiguration(true, false, -1, -1,
				ModelRendering.MODEL, true, DocExpansion.LIST, null, null,
				OperationsSorter.ALPHA, true, TagsSorter.ALPHA, null);
	}
}